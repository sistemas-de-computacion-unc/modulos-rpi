#include <linux/gpio.h>

#define A1  17 // 0
#define A2  18 // 1
#define A3  27 // 2
#define OE  22 // 3
#define LE  23 // 4
#define SDI 24 // 5
#define CLK 25 // 6

void pix_gpio_init(void){
  printk(KERN_INFO "PIX: starting gpio...");
  gpio_request(A1, "A1");
  gpio_request(A2, "A2");
  gpio_request(A3, "A3");

  gpio_request(OE, "OE");
  gpio_request(LE, "LE");
  gpio_request(SDI, "SDI");
  gpio_request(CLK, "CLK");

  gpio_direction_output(A1, 1);
  gpio_direction_output(A2, 1);
  gpio_direction_output(A3, 1);

  gpio_direction_output(OE, 1);
  gpio_direction_output(LE, 1);
  gpio_direction_output(SDI, 1);
  gpio_direction_output(CLK, 1);
  printk(KERN_INFO "PIX: starting gpio done.");
}

void pix_gpio_exit(void){
  printk(KERN_INFO "PIX: stopping gpio...");
  gpio_free(A1);
  gpio_free(A2);
  gpio_free(A3);

  gpio_free(OE);
  gpio_free(LE);
  gpio_free(SDI);
  gpio_free(CLK);
  printk(KERN_INFO "PIX: stopping gpio done.");
}
